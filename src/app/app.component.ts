import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'broker';
  shouldReloadNavigation: boolean;

  activateRoute(elementRef) {
    if(elementRef.action){
      elementRef.action.subscribe(event => {
        this.shouldReloadNavigation = event;
      });
    }
  }
}
