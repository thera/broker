import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NavigationComponent} from './components/shared/navigation.component';
import {DashboardComponent} from './modules/dashboard/dashboard.component';
import {ClientComponent} from './modules/clients/client.component';
import {LoginComponent} from './modules/authentication/login.component';
import {FormsModule} from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { ChartistModule } from 'ng-chartist';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {ShowComponent} from './modules/clients/show/show.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    ClientComponent,
      ShowComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgbModule,
    ChartsModule,
    ChartistModule,
    AppRoutingModule,
    NgxChartsModule,
    FormsModule
  ],
  providers: [
    NavigationComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
