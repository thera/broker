import {Component, Input, OnChanges} from '@angular/core';
import {AppRoutingModule} from '../../app-routing.module';
import {User} from '../../models/User';
import {Router} from '@angular/router';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent{

    @Input() public set reload(val: string) {
        this.isReloadTriggered = val;
        this.activateRoutes();
    }
    private isReloadTriggered;

    private appActive;

    private user: User;

    constructor(
        private route: Router
    ) {
        this.activateRoutes();
    }

    activateRoutes(){
        if (!localStorage.getItem('session')) {
            this.appActive = false;
        } else {
            this.user = JSON.parse(localStorage.getItem('session'));
            this.appActive = true;
        }
    }

    logout() {
        localStorage.removeItem('session');
        this.route.navigate(['login']);
        this.appActive = false;
    }
}
