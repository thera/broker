import {Component, EventEmitter, Output} from '@angular/core';
import {User} from '../../models/User';
import {AppRoutingModule} from '../../app-routing.module';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    @Output() action = new EventEmitter<boolean>();

    private user = new User();
    private errors = {};

    constructor(
        private route: Router
    ){
        if(localStorage.getItem('session')){
            route.navigate(['dashboard']);
        }
        this.user.email = "david@thera.tech";
        this.user.password = "123456";
    }

    login() {
        if(this.user.email && this.user.password){
            this.user.name = 'David Córdova';
            this.user.status = 1;
            localStorage.setItem('session', JSON.stringify(this.user));
            location.reload();
        } else {
            this.errors = {login: 'Debes llenar todos los campos'};
        }
    }
}
