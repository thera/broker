import {AfterViewInit, Component} from '@angular/core';
import * as c3 from 'c3';
import * as $ from 'jquery';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements AfterViewInit{
    constructor() {}

    private models = [
        {
            id: 1,
            model: 'VORT-X200',
            index: 9.5,
            worth: '10,504,230.09'
        },
        {
            id: 2,
            model: 'RT 250',
            index: 8.2,
            worth: '8,390,130.09'
        },
        {
            id: 3,
            model: 'FT250TS',
            index: 8.1,
            worth: '7,204,230.09'
        },
        {
            id: 4,
            model: 'FT150TS',
            index: 8.0,
            worth: '6,994,230.09'
        },
        {
            id: 5,
            model: 'V250EFI',
            index: 7.9,
            worth: '6,504,230.09'
        },
        {
            id: 5,
            model: '250Z',
            index: 7.9,
            worth: '5,504,230.09'
        },
        {
            id: 7,
            model: '200Z',
            index: 6.5,
            worth: '3,504,230.09'
        },
        {
            id: 8,
            model: 'FIERA250',
            index: 6.4,
            worth: '2,811,230.09'
        },
        {
            id: 9,
            model: 'TC250',
            index: 6.2,
            worth: '2,504,230.09'
        }
    ]

    public lineChartData: Array<any> = [
        { data: [3, 2, 5, 3, 17, 12], label: 'Balance $' }
    ];
    public lineChartLabels: Array<any> = [
        '2012',
        '2013',
        '2014',
        '2015',
        '2016',
        '2017'
    ];
    public lineChartOptions: any = {
        responsive: true,
        elements: {
            point: {
                radius: 2
            }
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        display: false
                    }
                }
            ],
            yAxes: [
                {
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        display: false
                    }
                }
            ]
        }
    };
    public lineChartColors: Array<any> = [
        {
            backgroundColor: 'transparent',
            borderColor: '#4dc8ff',
            pointBackgroundColor: '#4dc8ff',
            pointBorderColor: '#4dc8ff',
            pointHoverBackgroundColor: '#4dc8ff',
            pointHoverBorderColor: '#4dc8ff'
        }
    ];
    public lineChartLegend = false;
    public lineChartType = 'line';
}
