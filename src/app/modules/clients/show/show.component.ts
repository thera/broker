import { Component } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppRoutingModule} from '../../../app-routing.module';

@Component({
    selector: 'app-client-show',
    templateUrl: './show.component.html'
})
export class ShowComponent {
    private model = {
        id: this.randomNumber(),
        name: this.generateSellModel(),
        sell: this.generateSellId(),
        index: 9.5,
        worth: this.generateSellWorth()
    };
    private client = {
        name: this.generateSellBuyer(),
        uuid: this.route.snapshot.paramMap.get('id'),
        credit: this.generateSellCredit(),
        periodicity: this.generateSellPeriodicity(),
        worth: this.generateClientWorth()
    };

    constructor(private router: Router, private route: ActivatedRoute){

    }

    randomNumber(){
        return Math.floor((Math.random() * 10));
    }

    generateClientWorth(){
        return Math.floor((Math.random() * 10));
    }

    generateSellPeriodicity(){
        return Math.floor((Math.random() * (24-6)) + 1);
    }

    generateSellCredit(){
        let providers = [
            '',
            'Elektra',
            'Efectivo',
            'Banco Azteca'
        ];
        return providers[Math.floor((Math.random() * 3) + 1)];
    }

    generateSellBuyer(){
        let clients = [
                "Florencia Figueroa",
                "André Farías",
                "Maxi Riquelme",
                "Ever Ortega",
                "Bernardita Bustamante",
                "Antu Hernández",
                "Ervin Riquelme",
                "Elioth Alarcón",
                "Yeray López",
                "Luz Orellana",
                "Darwin Ortiz",
                "Maríajosé Pizarro",
                "Octavio Bustamante",
                "Maycol Salazar",
                "Angello Donoso",
                "Josue Silva",
                "Bernabé Fernández",
                "Josefina Pérez",
                "Arturo Rodríguez",
                "Juliette Alvarado",
                "Ariki San Martín",
                "Nicole Sáez",
                "Rebecca Garrido",
                "Lara Peña",
                "Celso Parra"
            ];
        return clients[Math.floor((Math.random() * 20) + 1)];
    }

    generateSellModel() {
        let models = [
            'FT150TS',
            'V250EFI',
            '250Z',
            '200Z',
            'FIERA250'
        ];
        return models[Math.floor((Math.random() * 4) + 1)];
    }

    generateSellId(){
        return Math.floor((Math.random() * 9999) + 1) +'-'+ Math.floor((Math.random() * 999999) + 1) +'-'+ Math.floor((Math.random() * 9999) + 1);
    }

    generateSellWorth(){
        return Math.floor((Math.random() * (99-10)) + 1) +','+ Math.floor((Math.random()*(999-100))) +'.'+ Math.floor((Math.random() * 99) + 1);
    }
}
