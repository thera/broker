import { Component } from '@angular/core';

@Component({
    selector: 'app-client-list',
    templateUrl: './client.component.html'
})
export class ClientComponent {
    private models = [
        {
            id: 9,
            model: 'VORT-X200',
            sell: this.generateSellId(),
            index: 9.5,
            worth: this.generateSellWorth()
        },
        {
            id: 5,
            model: 'RT 250',
            sell: this.generateSellId(),
            index: 8.2,
            worth: this.generateSellWorth()
        },
        {
            id: 2,
            model: 'FT250TS',
            sell: this.generateSellId(),
            index: 8.1,
            worth: this.generateSellWorth()
        },
        {
            id: 4,
            model: 'FT150TS',
            sell: this.generateSellId(),
            index: 8.0,
            worth: this.generateSellWorth()
        },
        {
            id: 1,
            model: 'V250EFI',
            sell: this.generateSellId(),
            index: 7.9,
            worth: this.generateSellWorth()
        },
        {
            id: 1,
            model: 'V250EFI',
            sell: this.generateSellId(),
            index: 7.9,
            worth: this.generateSellWorth()
        },
        {
            id: 5,
            model: 'RT 250',
            sell: this.generateSellId(),
            index: 6.5,
            worth: this.generateSellWorth()
        },
        {
            id: 8,
            model: 'FIERA250',
            sell: this.generateSellId(),
            index: 6.4,
            worth: this.generateSellWorth()
        },
        {
            id: 7,
            model: 'TC250',
            sell: this.generateSellId(),
            index: 6.2,
            worth: this.generateSellWorth()
        }
    ];

    generateSellId(){
        return Math.floor((Math.random() * 9999) + 1) +'-'+ Math.floor((Math.random() * 999999) + 1) +'-'+ Math.floor((Math.random() * 9999) + 1);
    }

    generateSellWorth(){
        return Math.floor((Math.random() * (99-10)) + 1) +','+ Math.floor((Math.random()*(999-100))) +'.'+ Math.floor((Math.random() * 99) + 1);
    }
}
