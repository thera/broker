import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './modules/dashboard/dashboard.component';
import {ClientComponent} from './modules/clients/client.component';
import {LoginComponent} from './modules/authentication/login.component';
import {ShowComponent} from './modules/clients/show/show.component';


const routes: Routes = [
  {path: '' , redirectTo: 'login', pathMatch: 'full'},
  {path: 'dashboard' , component: DashboardComponent},
  {path: 'clients' , component: ClientComponent},
  {path: 'clients/:id' , component: ShowComponent},
  {path: 'login' , component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
